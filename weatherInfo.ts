export class WeatherInfo {
                              //je n'ai créé qu'une seule classe et j'ai décidé de selectionner les
                              //attributs les plus importants pour des raisons de simplicité
  temp : number;
  humidity : number;
  windSpeed : number;
  cloud : number;
  rain : number;
  snow : number;
  weatherGeneral : string;
  weatherDescription : string;
  weatherImage : string;

}
