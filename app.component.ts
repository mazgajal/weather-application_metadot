import { Component } from '@angular/core';
import { WeatherInfo } from './weatherInfo';
import { WeatherSearchService } from './weatherSearch.service'



@Component({
  selector: 'app-root',
  template :`
  			<h1>{{title}}</h1>
  			<presentation></presentation>
  `,
  //templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [WeatherSearchService]
})
export class AppComponent {
  title = 'Weather Application';
}
