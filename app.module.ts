import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { WeatherSearchService } from './weatherSearch.service';
import { PresentationComponent } from './presentation.component';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent,
    PresentationComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [WeatherSearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
