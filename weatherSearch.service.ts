import { Component } from '@angular/core';
import { WeatherInfo } from './weatherInfo';

import { Http, Response } from '@angular/http';

import { Injectable } from '@angular/core';

import 'rxjs/Rx';


@Injectable()
export class WeatherSearchService {
  constructor(private http : Http){}

  searchByName(cityName : string, countryCode : string){
    return this.http.get('api.openweathermap.org/data/2.5/forecast?q=London&&APPID=535e1960e80c589f07ca8d0de06e2931').toPromise().then((res : Response) => res.json().data);
  }
}
