import { Component } from '@angular/core';
import { WeatherInfo } from './weatherInfo';
import { WeatherSearchService } from './weatherSearch.service';
import { Optional } from '@angular/core';
//import { OnInit } from '@angular/core';


@Component({
	selector : 'presentation',
	providers: [WeatherSearchService],
	template: `<h1>The weather</h1>
						<div *ngIf="infos">
						 <ul class="infos">
						 		<li *ngFor="let info of infos">
								{{info.temp}}
								</li>
							</ul>
						</div>
						<button (click)="getInfos()">bouton</button>`
})


export class PresentationComponent{ //implements OnInit {

	constructor(private searching : WeatherSearchService, @Optional() private infos : WeatherInfo[]){}

	jason : any;

	getInfos() : void {
		this.searching.searchByName('London','country').then(resPromise => this.jason = resPromise);
		console.log(this.jason);
	}

  TranslateInfos(): void{
	let elementList;
	let elementWeather;
	let index;
	let i :number =0;
	//var jason : any;
	//jason = this.searching.searchByName('London', 'countryCode');
	//console.log(this.jason);

	this.jason.list.forEach((elementList)=> {
		this.infos[i].temp = elementList.main.temp;
		this.infos[i].humidity = elementList.main.humidity;
		this.infos[i].cloud = elementList.clouds.all;
		this.infos[i].windSpeed = elementList.wind.speed;

		elementList.weather.forEach((elementWeather)=> {
			this.infos[i].weatherDescription = elementWeather.description;
			this.infos[i].weatherGeneral = elementWeather.main;
			//this.infos[i].weatherImage = elementWeather.icon;
		})
	})
}

}
